/**
 *                      *
 *    (    (  (       (  `
 *    )\   )\))(   '  )\))(      )  (
 *  (((_) ((_)()\ )  ((_)()\  ( /(  )\   (
 *  )\___ _(())\_)() (_()((_) )(_))((_)  )\ )
 * ((/ __|\ \((_)/ / |  \/  |((_)_  (_) _(_/(
 *  | (__  \ \/\/ /  | |\/| |/ _` | | || ' \))
 *   \___|  \_/\_/   |_|  |_|\__,_| |_||_||_|
 *
 */
// jquery stuff
jQuery(document).ready(function($){
	var slides = [
		'/img/slides/mgc_web-4.jpg',
		'/img/slides/mgc_web-1.jpg',
		'/img/slides/mgc_web-2.jpg',
		'/img/slides/mgc_web-3.jpg',
		'/img/slides/mgc_web-5.jpg',
	];


	for(var x = 0; x < slides.length; x++) {
		var current = '';
		if(x == 0) {
			current = 'current';
		}

		$('.slide-dots').append('<span class="slide-dot '+current+'" data-slide="'+x+'"></span>');
		$('.img-slider').append('<li class="slide" style="background: center center no-repeat url('+slides[x]+'); background-size: cover;"></li>');
	}

	// get the number of slides so we can connect to the dots for each one
	var slides_count = $('.img-slider').find('li').length;

	// set the starting styles, in this case just opacity
	$('.slide').each(function(){
		if($(this).index() > 0) {
			TweenMax.set($(this), {opacity: 0});
		}
	});

	// start with the first slide. in this case it's 1 and 0... i don't remember why
	var whichSlide = 1;

	// get the speed and pause values from the index.php
	var pause = $('.imgs-mother').data('pause');
	if(pause == 0 || pause == undefined) {
		pause = 5000;
	}

	var speed = $('.imgs-mother').data('speed');
	if(speed == 0 || speed == undefined) {
		speed = .5;
	}

	// handle slide transitions
	function slideChange(current, max){
		TweenMax.to($('.slide'), speed, { opacity: 0 });
		TweenMax.to($('.slide:eq('+current+')'), speed, { opacity: 1 });

		$('.slide-dot').removeClass('current');
		$('.slide-dot:eq('+current+')').addClass('current');
	};

	// start up the slider
	var slidesTimer = setInterval(function(){
		slideChange(whichSlide, slides_count);

		if(whichSlide >= (slides_count-1)) {
			whichSlide = 0;
		} else {
			whichSlide++;
		}
	}, pause);

	// when a dot is clicked, stop the timer set above, transition to the correct slide and then fire up the timer again.
	$('.slide-dot').on('click', function(){
		clearInterval(slidesTimer);

		var dotSlide = $(this).data('slide');

		TweenMax.to($('.slide'), speed, { opacity: 0 });
		TweenMax.to($('.slide:eq('+dotSlide+')'), speed, { opacity: 1 });

		$('.slide-dot').removeClass('current');
		$('.slide-dot:eq('+dotSlide+')').addClass('current');

		var new_whichSlide = dotSlide + 1;
		if(new_whichSlide >= slides_count) {
			new_whichSlide = 0;
		}

		slidesTimer = setInterval(function(){
			slideChange(new_whichSlide, slides_count);

			if(new_whichSlide >= (slides_count-1)) {
				new_whichSlide = 0;
			} else {
				new_whichSlide++;
			}
		}, pause);
	});

	// $(window).load(function(){

	// });

	// $(window).resize(function(){

	// });

	// $(window).scroll(function(){
		// do stuff
	// });
});
